package com.tcd.scss.powerr3.boardgame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {


    int[] squares = new int[63];

    void initialise(int[] sqs) {

        int i;
        for (i = 0; i < 64; i = i + 1) {
                squares[i] = 0;
        }
        //player one's squares
        squares[1] = 1;
        squares[3] = 1;
        squares[5] = 1;
        squares[7] = 1;
        squares[8] = 1;
        squares[10] = 1;
        squares[12] = 1;
        squares[14] = 1;
        squares[17] = 1;
        squares[19] = 1;
        squares[21] = 1;
        squares[23] = 1;

        //player two's squares
        squares[40] = 2;
        squares[42] = 2;
        squares[44] = 2;
        squares[46] = 2;
        squares[49] = 2;
        squares[51] = 2;
        squares[53] = 2;
        squares[55] = 2;
        squares[56] = 2;
        squares[58] = 2;
        squares[60] = 2;
        squares[62] = 2;



    }

    int input1 = 5;
    int input2 = 11;

    void checkInputP1(int args[]) {
        if (squares[input1] == 1) {
            /*
            bool done = false
            while(done == false){
            cout << not a valid sqaure, enter new square
            cin >> new
            if(squares[new]!=0)
                done = true;
               }
             */
        }

    }
    void movePieceP1(int args[]) {

        if (squares[input2] == 0) {
            squares[input2] = 1;    //new square
            squares[input1] = 0;    //old square
        } else if (squares[input2] == 2) {
            if (squares[input2 + (input2 - input1)] == 0) {
                squares[input2 + (input2 - input1)] = 1; //the square you have jumped to
                squares[input2] = 0;       //this is the square you have jumped over
                squares[input1] = 0;       //this is your old square
            } else {
                //invalid move
            }

        }
        else {
            //invalid move (because the square has a value of 1 [your own piece is there])
        }
    }

    void checkEndP1(int[] sqs) {

        int i;
        int counter = 0;

        for (i = 0; i < 64; i = i + 1) {
            if(squares[i] == 2){
                counter = counter +1;
            }
        }
        if(counter == 0){
            //game over (p1 wins)
        }
    }

    int input1p2 = 5;
    int input2p2 = 11;

    void checkInputp2(int args[]) {
        if (squares[input1p2] == 2) {
            /*
            bool done = false
            while(done == false){
            cout << not a valid sqaure, enter new square
            cin >> new
            if(squares[new]!=0)
                done = true;
               }
             */
        }

    }
    void movePiecep2(int args[]) {

        if (squares[input2p2] == 0) {
            squares[input2p2] = 2;    //new square
            squares[input1p2] = 0;    //old square
        } else if (squares[input2p2] == 1) {
            if (squares[input2p2 + (input2p2 - input1p2)] == 0) {
                squares[input2p2 + (input2p2 - input1p2)] = 2; //the square you have jumped to
                squares[input2p2] = 0;       //this is the square you have jumped over
                squares[input1p2] = 0;       //this is your old square
            } else {
                //invalid move
            }

        }
        else {
            //invalid move (because the square has a value of 1 [your own piece is there])
        }
    }

    void checkEndP2(int[] sqs) {

        int i;
        int counter = 0;

        for (i = 0; i < 64; i = i + 1) {
            if(squares[i] == 1){        //
                counter = counter +1;
            }
        }
        if(counter == 0){
            //game over (p2 wins)
        }
    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
